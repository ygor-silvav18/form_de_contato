<?php

namespace App\Http\Controllers;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class Send extends Controller
{
    private  $name;    
    private  $email;
    private  $message;    
    public function __construct(Request $request)
    {
        $this->name = $request->txt_name;
        $this->email = $request->txt_email;
        $this->message = $request->txt_message;
    }
    public function sendMail()
    {

        $bug = 10/0;
        $data = array(
            'name' => $this->name,
            'email' =>$this->email,
           'message'=>$this->message
        );

        Mail::to(config('mail.from.address'))
            ->send(new SendMail($data));
    }

}
