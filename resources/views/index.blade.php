<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>YDMania</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
    <div class="container">
            @csrf
            <div class="h-100 w-50 p-4 text-white bg-dark rounded-3 mx-auto">
                <form action="{{ url('/')}}" method="post">
                  {{csrf_field()}}
                    <div class="mb-3">
                        <h2>Entre em contato</h2>
                    </div>
                    @if(count($errors)> 0)
                        <div class="alert alert-danger d-flex align-items-center" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                            <div>
                                Dados incorretos!
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    @if($message = Session::get('success'))
                        <div class="alert alert-success d-flex align-items-center" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                            <div>
                                Obrigado
                                <p>{{$message}}</p>
                            </div>
                        </div>
                    @endif
                    @if($message = Session::get('error'))
                        <div class="alert alert-warning d-flex align-items-center" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                            <div>
                                {{$message}}
                            </div>
                        </div> 
                    @endif
                    <div class="mb-3">
                        <label for="txt_name" class="form-label">Nome:</label>
                        <input type="text" name="txt_name" class="form-control" tabindex="1" placeholder="Digite seu nome">
                    </div>
                    <div class="mb-3">
                        <label for="txt_email" class="form-label">Email:</label>
                        <input type="email" name="txt_email" class="form-control" tabindex="2" placeholder="Digite seu email">
                    </div>
                    <div class="mb-3">
                        <label for="txt_message" class="form-label">Menssagem:</label>
                        <textarea name="txt_message" class="form-control" tabindex="3" cols="20" rows="4" placeholder="sua menssagem"></textarea>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>                    
                </form>
            </div>
    </div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>
</html>